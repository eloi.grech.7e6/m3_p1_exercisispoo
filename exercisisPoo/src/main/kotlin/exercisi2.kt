/*
Fes un programa que llegeixi per pantalla un color i indica si forma part de l'arc de Sant Martí

Input
white

Output
false

 */

enum class RainbowColor {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    INDIGO,
    VIOLET
}

fun exercisi2(){
    println("##############")
    println("# EXERCISI 2 #")
    println("##############\n\n")

    val testColors = listOf("blue","green","red","white","purple","pink","grey","black")

    println("Check if colors are in the rainbow")
    for (col in testColors){
        println("$col: ${isRainbowColor(col)}")
    }
}

fun isRainbowColor(color: String): Boolean {
    return try {
        RainbowColor.valueOf(color.uppercase())
        true
    } catch (e: IllegalArgumentException) {
        false
    }
}
