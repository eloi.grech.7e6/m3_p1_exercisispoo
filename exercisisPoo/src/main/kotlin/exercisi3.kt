/*
3- InstrumentSimulator


Volem fer un petit programa d'emulació d'instruments.
Tenim els següents instruments.
Tambor
Triangle
Cada tambor té un to que pot ser A, O, U.
Els tambors amb A fan TAAAM, els tambors amb O fan TOOOM i els tambors amb U fan TUUUM.
Els triangles tenen una resonancia que va del 1 al 5.
Els triangles amb resonancia 1 fan TINC, els de dos TIINC, … i els de 5 TIIIIINC.
Fes les classes necessàries per poder executar el següent programa

 */

fun exercisi3(){
    println("##############")
    println("# EXERCISI 3 #")
    println("##############\n\n")



    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}
interface Instrument{
    fun makeSounds(times:Int)
}

class Triangle(tone:Int):Instrument{
    val tone = tone;
    override fun makeSounds(times: Int) {
        for (x in 0 until times){
            println("T"+"I".repeat(this.tone)+"M")

        }
    }
}

class Drump(tone:String):Instrument{
    val tone = tone
    override fun makeSounds(times: Int) {
        for (x in 0 until times) {
            println("T" + this.tone.uppercase().repeat(3) + "M")
        }
    }

}

