val col= mapOf(     //TEXT COLOR
    "red"     to "\u001b[31m",
    "reset"   to "\u001b[0m",
    "black"   to "\u001b[30m",
    "green"   to "\u001b[32m",
    "yellow"  to "\u001b[33m",
    "blue"    to "\u001b[34m",
    "magenta" to "\u001b[35m",
    "cyan"    to "\u001b[36m",
    "white"   to "\u001b[97m",
)


/**
 * @author Eloi Fàbrega
 * @version Versió 1, Data: 08/01/2023
 * Asks the user to input an integer within a given range and returns the input value.
 * @param min The minimum value that the user can input.
 * @param max The maximum value that the user can input.
 * @return The integer value that the user inputs.
 * @throws NumberFormatException if the user input is not a valid integer.
 */
fun askIntRange(min: Int, max: Int): Int {
    var usrSelection: Int = min
    var isValidInput = false
    while (!isValidInput) {
        val input = askIntSafe()
        if (input in min..max) {
            usrSelection = input
            isValidInput = true
        } else {
            println("${red("Introdueix un nombre entre $min i $max")}")
        }
    }
    return usrSelection
}

/**
 * Asks the user to input an integer and returns the input value if it is valid.
 * @return The integer value that the user inputs.
 * @throws NumberFormatException if the user input is not a valid integer.
 * @author Eloi Fàbrega
 * @version Versió 1, Data: 08/01/2023
 */
fun askIntSafe(): Int {
    var usrSelection: Int? = null
    var isValidInput = false
    while (!isValidInput) {
        try {
            usrSelection = readln().toInt()
            isValidInput = true
        } catch (e: NumberFormatException) {
            println("${red("Input invalid, intenta de nou")}")
        }
    }
    return usrSelection!!
}


/**
 * Returns the provided txt string formatted in red color using ANSI escape codes.
 * @param txt the string to format in red color
 * @return the red-formatted string
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun red(txt:String):String{
    return col["red"] + txt + col.get("reset")
}

/**
 * Returns the provided txt string formatted in green color using ANSI escape codes.
 * @param txt the string to format in red color
 * @return the green-formatted string
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun green(txt:String):String{
    return col["green"] + txt + col.get("reset")
}
