/*
1- StudentWithTextGrade

Volem enmagatzemar d'un estudiant, el seu nom, i la seva nota en format text. Una nota pot ser suspès, aprovat, bé, notable o excel·lent.

Fes un programa que crei 2 estudiants amb dues notes diferents i printa'ls per pantalla

Output

Student{name='Mar', textGrade=FAILED}
Student{name='Joan', textGrade=EXCELLENT}

 */

fun exercisi1(){

    println("##############")
    println("# EXERCISI 1 #")
    println("##############\n\n")

    val mar = Student("mar","suspès")
    val joan = Student("joan","excel·lent")

    println(mar)
    println(joan)


}

class Student(name:String,textGrade:String){
    var name ="";
    var textGrade = ""
    val accept = listOf("suspès","aprovat","bé","notable","excel·lent")


    init {

        if(!this.accept.contains(textGrade.lowercase())){
            throw IllegalArgumentException("Unexpected value on textGrade <$textGrade>\n valid inputs are: ${this.accept.joinToString(", ")}")
        }

        this.name = name;
        this.textGrade = textGrade;
    }

    override fun toString(): String {
        return "Student{name='$name',textGrade=${textGrade.uppercase()}}"
    }
}

