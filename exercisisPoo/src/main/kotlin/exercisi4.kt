/*
Volem fer un petit programa de preguntes.
Tenim dos tipus de preguntes:
 - opció múltiple
 - text lliure

Necessitaràs les següents classes
 - Question
 - FreeTextQuestion
 - MultipleChoiseQuestion
 - Quiz (conté una llista de preguntes)

Pas 1: Fes l'estructura de dades que et permeti guardar aquestes preguntes.
Pas 2: Fes un programa que, donat un quiz creat per codi, imprimeixi les preguntes.
Pas 3: Permet que l'usuari pugui respondre les preguntes.
Pas 4: Un cop acabat, imprimeix quantes respostes són correctes.


 */

fun exercisi4(){
    println("##############")
    println("# EXERCISI 4 #")
    println("##############\n\n")

    val quiz = Quiz()

    quiz.add(FreeTextQuestion("Quina es la capital d'Italia","Roma"))
    quiz.add(MultipleChoiceQuestion("Quina es la montanya mès alta del mon?", listOf("Montserrat","Teide","Everest"),2))

    quiz.ask()

}

interface Question{
    var correct:Boolean
    fun print()
    fun askResponse():Boolean
    fun showFail()

    fun siCorrect():Boolean{
        return this.correct
    }
}

class MultipleChoiceQuestion(text:String,options:List<String>,expectedResponse: Int):Question{
    val text = text
    val options = options
    var response = ""
    var responseIndex = -1
    val expectedResponse = expectedResponse
    override var correct = false
    var responded = false
    override fun print(){
        println(this.text)
        for(i in 0 until this.options.size){
            val opt = this.options[i]
            println("\t${i+1}) $opt")
        }
    }

    override fun askResponse(): Boolean {
        this.responded = true

        val usrInp = askIntRange(1,this.options.size)
        this.responseIndex = usrInp-1
        this.response = this.options[this.responseIndex]
        this.correct = this.responseIndex == this.expectedResponse
        return this.correct
    }

    private fun getNthChar(i:Int):String{
        return ('a'+i).toString()
    }

    override fun showFail() {
        print("\n\n")
        println(this.text)
        println(red("Has respos: $response"))
        println(green("Resposta correcta: ${this.options[expectedResponse]}"))
    }
}

class FreeTextQuestion(text:String,expectedResponse:String):Question{
    var response = ""
    val text = text
    val expectedResponse = expectedResponse
    override var correct = false
    var responded = false

    override fun print(){
        println(this.text)
    }

    override fun askResponse(): Boolean {
        this.responded = true
        this.response = readln()
        this.correct =  this.response == this.expectedResponse
        return this.correct
    }

    override fun showFail() {
        print("\n\n")
        println(this.text)
        println(red("Has respos: $response"))
        println(green("Resposta correcta: ${this.expectedResponse}"))
    }
}

class Quiz(){
    val list = mutableListOf<Question>()

    fun add(q:Question){
        list.add(q)
    }

    fun ask(){
        for(question in this.list){
            question.print()
            val response = question.askResponse()
            //println(green("User has responded: $response"))
        }

        val fallidas = this.list.toList().filter { !it.correct }
        println("Has respos ${list.size - fallidas.size} preguntes correctament")

        for(fallo in fallidas){
            fallo.showFail()
        }
    }
}
