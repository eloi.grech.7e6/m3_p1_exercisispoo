import java.util.*

fun exercisi5(){
    val controller = GymControlManualReader()

    for (x in 0..8){
        val move = controller.nextId()
        println(move)
    }
}

interface GymControlReader{
    fun nextId():String
}

class GymControlManualReader(private val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    private val ids = mutableSetOf<String>()

    override fun nextId(): String {
        print("Enter ID: ")
        val id = scanner.next()
        return if (ids.contains(id)) {
            ids.remove(id)
            "Sortida"
        } else {
            ids.add(id)
            "Entrada"
        }
    }

}